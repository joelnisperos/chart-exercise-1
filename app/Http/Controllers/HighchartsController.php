<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HighchartsController extends Controller
{
    //
    public function demo1(){
        return view('highcharts.demo1');
    }

    public function demo2(){
        return view('highcharts.demo2');
    }
}
