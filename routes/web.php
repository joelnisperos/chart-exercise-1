<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

Route::get('/demo1', 'HighchartsController@demo1');

Route::get('/demo2', 'HighchartsController@demo2');
*/
Route::get('/highcharts/demo1', ['as' => 'highcharts_demo_1', 'uses' => 'HighchartsController@demo1']);

Route::get('/highcharts/demo2', ['as' => 'highcharts_demo_2', 'uses' => 'HighchartsController@demo2']);

