<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Charts-->
        <script src="https://code.highcharts.com/highcharts.src.js"></script>
 
    </head>
    <body>
            <script src="../../code/highcharts.js"></script>
            <script src="../../code/modules/exporting.js"></script>
            
            <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
            
            <script type="text/javascript">
            
            
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Economic Profit %',
        align: 'left'
    },
    xAxis: {
        categories: ['Monday', 'Tuesday', 'Wednesday', 'Thuesday', 'Friday']
    },
    yAxis: {
        min: 0,
        title: {
            text: ''
        },
        labels: {
    			formatter: function() {
       		return this.value *100+"%";
    }
  },
    },
    tooltip: {
    formatter: function() {
      return '<span style="color: ' + this.series.color + '">' + this.series.name +
        '</span>: <b>' + Highcharts.numberFormat((this.y * 100), 2, '.') + '%</b>';
    }
      
    },
    plotOptions: {
        column: {
            stacking: 'normal'
        }
    },
    series: [{
                    name: 'Papa Bear',
                    data: [0.124, 0.6693, 1.252, 0.3463, 0.183],
                    color: '#D8692F'
                }, {
                    name: 'Mama Bear',
                    data: [0.997, 0.111, 0.637, 0.551, 1.116],
                    color: '#e0885a'
                }
                ]
});
            
        </script>
                
    </body>
</html>
