<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Charts-->
        <script src="https://code.highcharts.com/highcharts.src.js"></script>
 
    </head>
    <body>
            <script src="../../code/highcharts.js"></script>
            <script src="../../code/modules/exporting.js"></script>
            
            <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
            
            <script type="text/javascript">
            
            Highcharts.chart('container', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Economic Profit %',
                    align: 'left',
                    style: {
                    fontWeight: 'bold',
                    }
                },
                xAxis: {
                    categories: ['2011', '2012', '2013', '2014', '2015']
                },
                yAxis: {
                    min: 0,
                    title: {
                    text: ''
                    },
                    stackLabels: {
                    enabled: false
                    }
                },
                legend: {
                    reversed: true
                },
                tooltip: {
                    pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
                    shared: true
                },
                plotOptions: {
                    column: {
                    stacking: 'normal'
                    }
                },
                series: [{
                    name: 'Kenneth',
                    data: [12, 15, 4, 16, 9],
                    color: '#D8692F'
                }, {
                    name: 'Ed',
                    data: [8, 22, 9, 12, 7],
                    color: '#e0885a'
                }, {
                    name: 'Gillian',
                    data: [19, 5, 1, 21, 18],
                    color: '#DA524C'
                }]
            });
            
        </script>
                
    </body>
</html>
